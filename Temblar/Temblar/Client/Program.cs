using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Temblar.Client.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Blazored.LocalStorage;
using Blazored.Toast;
using Blazored.Toast.Services;

namespace Temblar.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.Services.AddSingleton< NotificationService>();
            builder.Services.AddSingleton<AuthorizationService>();
            builder.Services.AddSingleton<SourceService>();
            builder.Services.AddSingleton<TemblarAppState>();
            builder.Services.AddSingleton<LocalStorageService>();
            builder.Services.AddSingleton<StorageService>();
            builder.Services.AddSingleton<SignOutSessionStateManager>();
            builder.Services.AddSingleton<WindowService>();
            builder.Services.AddSingleton<HttpErrorService>();
            builder.Services.AddBlazoredToast();

            Console.WriteLine(builder.Configuration.GetValue<string>("AUTHORITY"));

            builder.RootComponents.Add<App>("app");
          
            builder.Services.AddHttpClient("Temblar.ServerAPI", client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
                .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();
     
            builder.Services.AddSingleton(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("Temblar.ServerAPI"));


            
            builder.Services.AddMsalAuthentication(options =>
                {
                    var authentication = options.ProviderOptions.Authentication;
                    authentication.Authority = "https://login.microsoftonline.com/ead5a9b8-4a2d-4d79-b8f5-3beff6730b27";
                    authentication.ClientId = "bd1fd829-80bf-4659-879e-1adffb84dd60";
                    authentication.ValidateAuthority = true;

                    var DefaultAccessTokenScopes = options.ProviderOptions.DefaultAccessTokenScopes;
                    DefaultAccessTokenScopes.Add("bd1fd829-80bf-4659-879e-1adffb84dd60");

                });

            await builder.Build().RunAsync();
        }
    }
}
