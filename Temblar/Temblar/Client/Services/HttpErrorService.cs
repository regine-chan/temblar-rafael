﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Temblar.Client.Services
{
    public class HttpErrorService
    {
        private readonly TemblarAppState appState;

        private string UNAUTHORIZED { get; } = "Kunne ikke autentisere deg, logg inn og prøv igjen.";

        private string INTERNAL_SERVER_ERROR { get; } = "Kunne ikke behandle forespørselen, prøv igjen senere.";

        private string NOT_FOUND { get; } = "Kunne ikke finne innholdet, prøv igjen senere";

        private string BAD_REQUEST { get; } = "Feil med forespørsel, prøv igjen";

        private Func<HttpStatusCode, string> UNHANDLED = code => $"Ubehandlet feilkode: {code}";

        public HttpErrorService(TemblarAppState appState)
        {
            this.appState = appState;
        }

        public void HandleError(HttpStatusCode code)
        {
            switch (code)
            {
                case HttpStatusCode.BadRequest:
                    appState.errorMessage = BAD_REQUEST;
                    break;
                case HttpStatusCode.NotFound:
                    appState.errorMessage = NOT_FOUND;
                    break;
                case HttpStatusCode.InternalServerError:
                    appState.errorMessage = INTERNAL_SERVER_ERROR;
                    break;
                case HttpStatusCode.Unauthorized:
                    appState.errorMessage = UNAUTHORIZED;
                    break;
                default:
                    appState.errorMessage = UNHANDLED(code);
                    break;
            }
        }
    }
}
