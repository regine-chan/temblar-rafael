﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Temblar.Shared.Commands;
using Temblar.Shared.DTOs;

namespace Temblar.Client.Services
{
    public class NotificationService
    {
        private readonly HttpClient httpClient;

        private readonly TemblarAppState appState;

        private readonly HttpErrorService httpErrorService;

        public List<PushNotificationDto> NotReadNotifiations { get; set; }

        public List<PushNotificationDto> ReadNotifications { get; set; }

        public int UnseenNotificationCount { get; set; }

        public NotificationService(HttpClient httpClient, TemblarAppState appState, HttpErrorService httpErrorService)
        {
            this.httpClient = httpClient;
            this.appState = appState;
            this.httpErrorService = httpErrorService;
            UnseenNotificationCount = 0;
            NotReadNotifiations = new List<PushNotificationDto>();
            ReadNotifications = new List<PushNotificationDto>();
        }

        public async Task MarkAllMyNotificationsAsRead()
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsync("/api/Notifications/MarkAllMyNotificationsAsRead", null);

                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task MarkAllNotificationsAsSeenCommand(Guid FromNotificationAndOlderId)
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsJsonAsync("/api/Notifications/MarkAllMyNotificationsAsSeen", new MarkAllNotificationsAsSeenCommand { FromNotificationAndOlderId = FromNotificationAndOlderId });


                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task MarkAsRead(List<Guid> ReadIds)
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.PutAsJsonAsync("/api/Notifications/MarkAsRead", new MarkNotificationsAsReadCommand { ReadIds = ReadIds });


                if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task MyLatestNotifications()
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetAsync("/api/Notifications/MyLatestNotifications");

                if (result.StatusCode == HttpStatusCode.OK)
                {
                    appState.notReadNotifications = await result.Content.ReadFromJsonAsync<List<PushNotificationDto>>();
                }
                else if(result.StatusCode == HttpStatusCode.NoContent)
                {
                    appState.notReadNotifications = new List<PushNotificationDto>();
                }
                else if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task MyNotificationsArchive()
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetAsync("/api/Notifications/MyNotificationsArchive");

                if (result.StatusCode == HttpStatusCode.OK)
                {
                    appState.readNotifications = await result.Content.ReadFromJsonAsync<List<PushNotificationDto>>();
                }
                else if (result.StatusCode == HttpStatusCode.NoContent)
                {
                    appState.readNotifications = new List<PushNotificationDto>();
                }
                else if (!result.IsSuccessStatusCode)
                {
                    httpErrorService.HandleError(result.StatusCode);
                }
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

        public async Task MyUnseenNotificationCount()
        {
            appState.isLoading = true;
            appState.errorMessage = "";
            try
            {
                var result = await httpClient.GetFromJsonAsync<int>("/api/Notifications/MyUnseenCustomNotificationCount");
                UnseenNotificationCount = result;
                appState.isLoading = false;
            }
            catch (Exception e)
            {
                appState.errorMessage = e.Message;
            }
            finally
            {
                appState.isLoading = false;
            }
        }

    }
}
