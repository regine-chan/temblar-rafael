﻿
using System.Threading.Tasks;
using Microsoft.JSInterop;


namespace Temblar.Client.Services
{
    // Service to store MSAL Id token in localstorage to avoid logging in every time when token is valid
    public class StorageService
    {
       
        private readonly IJSRuntime JSRuntime;

        public StorageService(IJSRuntime jSRuntime)
        {
       
            JSRuntime = jSRuntime;
        }

        // Retrieves id token from session storage and stores it in local storage
        public async Task SetTokenFromSessionToLocal()
        {
            await JSRuntime.InvokeVoidAsync("setTokenFromSessionToLocal");
        }

        // Retrieves id token from local storage and stores it in session storage
        public async Task<bool> SetTokenFromLocalToSession()
        {
            bool tokenIsSet = await JSRuntime.InvokeAsync<bool>("setTokenFromLocalToSession");
            return tokenIsSet;
        }
    }
}
