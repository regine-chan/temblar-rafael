﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace Temblar.Client.Services
{
    // Service for using window api 
    public class WindowService
    {
        private readonly IJSRuntime JSRuntime;
        
        public WindowService(IJSRuntime jSRuntime)
        {
            JSRuntime = jSRuntime;
        }

        // Checks if app is in standalone mode
        public async Task<bool> isStandAlone()
        {
           bool checkStandAlone=  await JSRuntime.InvokeAsync<bool>("isStandAlone");
           return checkStandAlone;
        }

        // Checks if device is on iOS 
        public async Task<bool> IsiOS()
        {
            bool checkIos = await JSRuntime.InvokeAsync<bool>("isiOS");
            return checkIos;
        }

        // Checks if device is on Android
        public async Task<bool> IsAndroid()
        {
            bool checkAndroid = await JSRuntime.InvokeAsync<bool>("isAndroid");
            return checkAndroid;
        }
    }
}
