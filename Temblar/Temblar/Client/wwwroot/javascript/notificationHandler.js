﻿window.requestNotificationPermission = async () => {
    Notification.requestPermission(status => status);
}

window.displayNotification = async message => {
    if (Notification.permission) {
        navigator.serviceWorker.getRegistration().then(reg => {
            reg.showNotification(message);
        });
    }
}