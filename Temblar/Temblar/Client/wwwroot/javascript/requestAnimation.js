﻿/*
    https://developers.google.com/web/fundamentals/design-and-ux/input/touch/
    for resources on touch events
*/ 

(function () {

    // Shim for requestAnimationFrame from Paul Irishpaul ir
      // http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
      window.requestAnimFrame = (function(){
        'use strict';

        return  window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                function( callback ){
                  window.setTimeout(callback, 1000 / 60);
                };
      })();

    let rafPending = false;
    let swipeFrontElement;
    let initialTouchPos = null;
    let lastTouchPos = null;

    window.blazorTouchEvent = {

        setSwipeFrontElement: (id) => {
            swipeFrontElement = document.getElementById(id);
            swipeFrontElement.addEventListener('touchstart', handleGestureStart, true);
            swipeFrontElement.addEventListener('touchmove', handleGestureMove, true);
            swipeFrontElement.addEventListener('touchend', handleGestureEnd, true);
            swipeFrontElement.addEventListener('touchcancel', handleGestureEnd, true);
            swipeFrontElement.style.animation = "";
            swipeFrontElement.style.position = 'relative';
        },
    } 

    /* // [START handle-start-gesture] */
    // Handle the start of gestures
    handleGestureStart = function (evt) {
        evt.preventDefault();

        if (evt.touches && evt.touches.length > 1) {
            return;
        }

        initialTouchPos = getGesturePointFromEvent(evt);
        console.log(initialTouchPos);
        swipeFrontElement.style.transition = 'initial';
    };
    /* // [END handle-start-gesture] */

    // Handle move gestures
    //
    /* // [START handle-move] */
    handleGestureMove = function (evt) {
        evt.preventDefault();

        if (!initialTouchPos) {
            return;
        }

        lastTouchPos = getGesturePointFromEvent(evt);

        if (rafPending) {
            return;
        }

        rafPending = true;

        window.requestAnimFrame(onAnimFrame);
    };
    /* // [END handle-move] */

    /* // [START handle-end-gesture] */
    // Handle end gestures
    handleGestureEnd = function (evt) {
        evt.preventDefault();

        if (evt.touches && evt.touches.length > 0) {
            return;
        }

        rafPending = false;

        initialPosition();
    };
        /* // [END handle-end-gesture] */

    function getGesturePointFromEvent(evt) {
        var point = {};
        point.x = evt.targetTouches[0].clientX;
        point.y = evt.targetTouches[0].clientY;
        return point;
    };

    function initialPosition(){
        transformStyle = 'translate(0)';
        swipeFrontElement.style.transition = "all 150ms ease-out";
        swipeFrontElement.style.backgroundColor = "white";
        swipeFrontElement.style.color = "black";
        setTransformStyle(transformStyle);
    }

    function setTransformStyle(transformStyle) {
        swipeFrontElement.style.webkitTransform = transformStyle;
        swipeFrontElement.style.MozTransform = transformStyle;
        swipeFrontElement.style.msTransform = transformStyle;
        swipeFrontElement.style.transform = transformStyle; 
    }


    function onAnimFrame() {
        if(!rafPending) {
            return;
          }

        var differenceInX = initialTouchPos.x - lastTouchPos.x;

        if (differenceInX > 100) {
            swipeFrontElement.style.backgroundColor = "red";
            swipeFrontElement.style.color = "white";
        }

          var newXTransform = (-differenceInX)+'px';
          var transformStyle = 'translateX('+newXTransform+')';
          setTransformStyle(transformStyle);

          rafPending = false;
    }
})();