﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Temblar.Shared.Commands;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("/api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly TemblarDbContext ctx;
        private readonly IHubContext<NotificationHub> hubContext;

        public AuthorizationController(TemblarDbContext ctx, IHubContext<NotificationHub> hubContext)
        {
            this.ctx = ctx;
            this.hubContext = hubContext;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearEvent()
        {
            ctx.ClientDevice.RemoveRange(ctx.ClientDevice);
            await ctx.SaveChangesAsync();
            return NoContent();
        }

        [HttpPut("client")]
        public async Task<ActionResult> AddClientDevice([FromBody] ClientDevice clientDevice)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if(identity != null && clientDevice != null)
            {
                var email = identity.FindFirst("preferred_username").Value;

                try
                {
                    TemblarUser temblarUser = await ctx.TemblarUser.Where(tu => tu.Username == email).FirstOrDefaultAsync();
                    if (temblarUser != null)
                    {
                        clientDevice.TemblarUserId = temblarUser.Id;
                        ctx.ClientDevice.Add(clientDevice);
                        int changes = await ctx.SaveChangesAsync();

                        if (changes > 0)
                        {
                            return NoContent();
                        }
                    }
                }
                catch(Exception e)
                {
                    return StatusCode(500, e.Message);
                }
                
            }
            return NotFound();
        }

        // On authenticated user, call this to add or check if added to db
        [HttpGet]
        public async Task<ActionResult<TemblarUser>> SaveUserIfExists()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var email = identity.FindFirst("preferred_username").Value;
                TemblarUser tu = null;
                try
                {
                    tu = await ctx.TemblarUser.Where(t => t.Username == email).FirstOrDefaultAsync();
                    if (tu != null)
                    {
                        return Ok(tu);
                    }
                    else
                    {
                        tu = new TemblarUser { Username = email };
                        ctx.TemblarUser.Add(tu);
                        int result = await ctx.SaveChangesAsync();
                        if (result > 0)
                        {
                            return Ok(tu);
                        }
 
                    }
                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

       
    }
}