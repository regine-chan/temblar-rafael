﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientDeviceController : ControllerBase
    {
        private readonly TemblarDbContext _context;

        public ClientDeviceController(TemblarDbContext context)
        {
            _context = context;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearClientDevices()
        {
            _context.ClientDevice.RemoveRange(_context.ClientDevice);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/ClientDevices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientDevice>>> GetClientDevice()
        {
            return await _context.ClientDevice.Include(n => n.TemblarUser).ToListAsync();
        }

        // GET: api/ClientDevices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClientDevice>> GetClientDevice(Guid id)
        {
            var clientDevice = await _context.ClientDevice.FindAsync(id);

            if (clientDevice == null)
            {
                return NotFound();
            }

            return clientDevice;
        }

        // PUT: api/ClientDevices/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClientDevice(Guid id, ClientDevice clientDevice)
        {
            if (id != clientDevice.Id)
            {
                return BadRequest();
            }

            _context.Entry(clientDevice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientDeviceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ClientDevices
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ClientDevice>> PostClientDevice(ClientDevice clientDevice)
        {
            _context.ClientDevice.Add(clientDevice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClientDevice", new { id = clientDevice.Id }, clientDevice);
        }

        // DELETE: api/ClientDevices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClientDevice>> DeleteClientDevice(Guid id)
        {
            var clientDevice = await _context.ClientDevice.FindAsync(id);
            if (clientDevice == null)
            {
                return NotFound();
            }

            _context.ClientDevice.Remove(clientDevice);
            await _context.SaveChangesAsync();

            return clientDevice;
        }

        private bool ClientDeviceExists(Guid id)
        {
            return _context.ClientDevice.Any(e => e.Id == id);
        }
    }
}
