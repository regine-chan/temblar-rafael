﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("/api/[controller]")]
    [ApiController]
    public class CustomNotificationsController : ControllerBase
    {
        private readonly TemblarDbContext _context;

        public CustomNotificationsController(TemblarDbContext context)
        {
            _context = context;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearCustomNotifications()
        {
            _context.CustomNotification.RemoveRange(_context.CustomNotification);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/CustomNotifications
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomNotification>>> GetCustomNotification()
        {
            return await _context.CustomNotification.Include(n => n.TemblarUser).Include(n => n.Event).ToListAsync();
        }

        // GET: api/CustomNotifications/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomNotification>> GetCustomNotification(Guid id)
        {
            var customNotification = await _context.CustomNotification.FindAsync(id);

            if (customNotification == null)
            {
                return NotFound();
            }

            return customNotification;
        }

        // PUT: api/CustomNotifications/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomNotification(Guid id, CustomNotification customNotification)
        {
            if (id != customNotification.Id)
            {
                return BadRequest();
            }

            _context.Entry(customNotification).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomNotificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CustomNotifications
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CustomNotification>> PostCustomNotification(CustomNotification customNotification)
        {
            _context.CustomNotification.Add(customNotification);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCustomNotification", new { id = customNotification.Id }, customNotification);
        }

        // DELETE: api/CustomNotifications/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CustomNotification>> DeleteCustomNotification(Guid id)
        {
            var customNotification = await _context.CustomNotification.FindAsync(id);
            if (customNotification == null)
            {
                return NotFound();
            }

            _context.CustomNotification.Remove(customNotification);
            await _context.SaveChangesAsync();

            return customNotification;
        }

        private bool CustomNotificationExists(Guid id)
        {
            return _context.CustomNotification.Any(e => e.Id == id);
        }
    }
}