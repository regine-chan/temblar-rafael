﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Server.Handlers;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("/api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly TemblarDbContext _context;
        private readonly NewEventHandler eventHandler;

        public EventsController(TemblarDbContext context)
        {
            _context = context;
            eventHandler = new NewEventHandler(_context);
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearEvent()
        {
            _context.Event.RemoveRange(_context.Event);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/Events
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Event>>> GetEvent()
        {
            return await _context.Event.Include(e => e.EventType).Include(e => e.Source).ToListAsync();
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Event>> GetEvent(Guid id)
        {
            var @event = await _context.Event.FindAsync(id);

            if (@event == null)
            {
                return NotFound();
            }

            return @event;
        }

        // PUT: api/Events/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEvent(Guid id, Event @event)
        {
            if (id != @event.Id)
            {
                return BadRequest();
            }

            _context.Entry(@event).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Events
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Event>> PostEvent(Event @event)
        {
            if (@event.EventType != null)
            {
                _context.Entry(@event.EventType).State = EntityState.Unchanged;
            }
            else if (@event.Source != null)
            {
                _context.Entry(@event.Source).State = EntityState.Unchanged;
            }
            @event.Id = Guid.NewGuid();
            @event.TriggeredAt = new DateTimeOffset(DateTime.Now);
            @event.UserInterfaceUrl = "/api/Events/" + @event.Id;
            _context.Event.Add(@event);
            var result = await _context.SaveChangesAsync();
            if (result > 0)
            {
                try
                {
                    var success = await eventHandler.OnNewEvent(@event);
                    if (success)
                    {
                        return Ok();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return BadRequest();
                }
            }
            return BadRequest();
        }

        // DELETE: api/Events/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Event>> DeleteEvent(Guid id)
        {
            var @event = await _context.Event.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            _context.Event.Remove(@event);
            await _context.SaveChangesAsync();

            return @event;
        }

        private bool EventExists(Guid id)
        {
            return _context.Event.Any(e => e.Id == id);
        }
    }
}