﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("/api/[controller]")]
    [ApiController]
    public class SourceSubscriptionsController : ControllerBase
    {
        private readonly TemblarDbContext _context;

        public SourceSubscriptionsController(TemblarDbContext context)
        {
            _context = context;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearSourceSubscriptions()
        {
            _context.SourceSubscription.RemoveRange(_context.SourceSubscription);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/SourceSubscriptions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SourceSubscription>>> GetSourceSubscription()
        {
            return await _context.SourceSubscription.Include(s => s.TemblarUser).Include(s => s.Source).ToListAsync();
        }

        // GET: api/SourceSubscriptions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SourceSubscription>> GetSourceSubscription(Guid id)
        {
            var sourceSubscription = await _context.SourceSubscription.FindAsync(id);

            if (sourceSubscription == null)
            {
                return NotFound();
            }

            return sourceSubscription;
        }

        // PUT: api/SourceSubscriptions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{sourceId}&{temblarUserid}")]
        public async Task<IActionResult> PutSourceSubscription(Guid sourceId, Guid temblarUserId, SourceSubscription sourceSubscription)
        {
            if (temblarUserId != sourceSubscription.TemblarUserId || sourceId != sourceSubscription.SourceId)
            {
                return BadRequest();
            }

            _context.Entry(sourceSubscription).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SourceSubscriptionExists(sourceId, temblarUserId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SourceSubscriptions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SourceSubscription>> PostSourceSubscription(SourceSubscription sourceSubscription)
        {
            _context.Entry(sourceSubscription.Source).State = EntityState.Unchanged;
            _context.Entry(sourceSubscription.TemblarUser).State = EntityState.Unchanged;

            _context.SourceSubscription.Add(sourceSubscription);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SourceSubscriptionExists(sourceSubscription.SourceId, sourceSubscription.TemblarUserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSourceSubscription", new { id = sourceSubscription.TemblarUserId }, sourceSubscription);
        }

        // DELETE: api/SourceSubscriptions/5
        [HttpDelete("{sourceId}&{temblarUserId}")]
        public async Task<ActionResult<SourceSubscription>> DeleteSourceSubscription(Guid sourceId, Guid temblarUserId)
        {
            var sourceSubscription = await _context.SourceSubscription.Where(s => s.SourceId == sourceId).Where(s => s.TemblarUserId == temblarUserId).FirstAsync();
            if (sourceSubscription == null)
            {
                return NotFound();
            }

            _context.SourceSubscription.Remove(sourceSubscription);
            await _context.SaveChangesAsync();

            return sourceSubscription;
        }

        private bool SourceSubscriptionExists(Guid sourceId, Guid temblarUserId)
        {
            return _context.SourceSubscription.Where(e => e.TemblarUserId == temblarUserId).Where(e => e.SourceId == sourceId).Any();
        }
    }
}