﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("/api/[controller]")]
    [ApiController]
    public class SubscriptionsController : ControllerBase
    {
        private readonly TemblarDbContext _context;

        public SubscriptionsController(TemblarDbContext context)
        {
            _context = context;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearSubscriptions()
        {
            _context.Subscription.RemoveRange(_context.Subscription);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/Subscriptions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Subscription>>> GetSubscription()
        {
            return await _context.Subscription.Include(s => s.TemblarUser).Include(s => s.EventType).ToListAsync();
        }

        // GET: api/Subscriptions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Subscription>> GetSubscription(Guid id)
        {
            var subscription = await _context.Subscription.FindAsync(id);

            if (subscription == null)
            {
                return NotFound();
            }

            return subscription;
        }

        // PUT: api/Subscriptions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{eventTypeId}&{temblarUserId}")]
        public async Task<IActionResult> PutSubscription(Guid eventTypeId, Guid temblarUserId, Subscription subscription)
        {
            if (temblarUserId != subscription.TemblarUserId || eventTypeId != subscription.EventTypeId)
            {
                return BadRequest();
            }

            _context.Entry(subscription).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubscriptionExists(eventTypeId, temblarUserId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subscriptions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Subscription>> PostSubscription(Subscription subscription)
        {
            _context.Entry(subscription.EventType).State = EntityState.Unchanged;
            _context.Entry(subscription.TemblarUser).State = EntityState.Unchanged;
            _context.Subscription.Add(subscription);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubscriptionExists(subscription.EventTypeId, subscription.TemblarUserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubscription", new { id = subscription.TemblarUserId }, subscription);
        }

        // DELETE: api/Subscriptions/5
        [HttpDelete("{eventTypeId}&{temblarUserId}")]
        public async Task<ActionResult<Subscription>> DeleteSubscription(Guid eventTypeId, Guid temblarUserId)
        {
            var subscription = await _context.Subscription.Where(e => e.EventTypeId == eventTypeId).Where(e => e.TemblarUserId == temblarUserId).FirstAsync();
            if (subscription == null)
            {
                return NotFound();
            }

            _context.Subscription.Remove(subscription);
            await _context.SaveChangesAsync();

            return subscription;
        }

        private bool SubscriptionExists(Guid eventTypeId, Guid temblarUserId)
        {
            return _context.Subscription.Where(e => e.EventTypeId == eventTypeId).Where(e => e.TemblarUserId == temblarUserId).Any();
        }
    }
}