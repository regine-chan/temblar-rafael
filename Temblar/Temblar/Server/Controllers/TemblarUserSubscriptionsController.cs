﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Commands;
using Temblar.Shared.DTOs;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [Route("/api/")]
    [ApiController]
    public class TemblarUserSubscriptionsController : ControllerBase
    {
        private readonly TemblarDbContext _context;
    
        public TemblarUserSubscriptionsController(TemblarDbContext context)
        {
            _context = context;
        }

        // GET: api/personalsubscribers/guid
        [HttpGet("personalsubscribers/{temblarUserId}")]
        public async Task<ActionResult<PersonalSubscriberDTO>> GetAllSubscribedResources(Guid temblarUserId)
        {
            if(temblarUserId == null)
            {
                return BadRequest();
            }
            // Creates Hashset for temporary storing sources
            HashSet<string> sources = new HashSet<string>();

            try
            {
                // Gets temblar user
                TemblarUser temblarUser = await _context.TemblarUser.Include(tu => tu.SourceSubscriptions).Where(tu => tu.Id == temblarUserId).FirstOrDefaultAsync();
                if(temblarUser == null)
                {
                    return NotFound();
                }
                // Gets source subscriptions
                List<SourceSubscription> sourceSubscriptions = await _context.SourceSubscription.Where(ss => ss.TemblarUserId == temblarUserId).Include(s => s.Source).ToListAsync();

                // Adds source subscriptions to source
                foreach (SourceSubscription ss in sourceSubscriptions)
                {
                    sources.Add(ss.Source.SourceName);
                }

                // Creates SourceSubscriptionsDTO object
                PersonalSubscriberDTO sourceSubscriptionsDTO = new PersonalSubscriberDTO();

                // Updates values
                sourceSubscriptionsDTO.Id = temblarUserId;
                sourceSubscriptionsDTO.Name = temblarUser.Username;
                sourceSubscriptionsDTO.SubscribedSources = sources;

                // Returns SourceSubscriptionsDTO
                return Ok(sourceSubscriptionsDTO);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/sources
        [HttpGet("sources")]
        public async Task<ActionResult<List<string>>> GetAllSources()
        {
            // Creates list for storing sources as strings
            List<string> sourceList = new List<string>();

            try
            {
                // Gets all sources
                List<Source> sources = await _context.Source.ToListAsync();
                if(sources == null || sources.Count == 0)
                {
                    return NotFound();
                }

                // Adds sources to sourceList
                foreach (Source s in sources)
                {
                    sourceList.Add(s.SourceName);
                }
                // Returns sourceList
                return Ok(sourceList);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // PUT: api/guid/subscribetosource
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{subscriberId}/subscribetosource")]
        public async Task<IActionResult> SubscribeSourceSubscription(Guid subscriberId, [FromBody] SubscribeToSourceCommand subscribeToSourceCommand)
        {
            if (subscribeToSourceCommand == null || subscriberId == null)
            {
                return BadRequest();
            }

            Source src = null;
            int changes = 0;

            try
            {
                src = await _context.Source.Where(s => s.SourceName == subscribeToSourceCommand.Source).FirstOrDefaultAsync();
                if (src == null)
                {
                    return NotFound();
                }

                _context.SourceSubscription.Add(new SourceSubscription { SourceId = src.Id, TemblarUserId = subscriberId });

                changes = await _context.SaveChangesAsync();

                if (changes == 0)
                {
                    return StatusCode(304);
                }
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            // Gets temblar user

            return NoContent();
        }

        // PUT: api/guid/unsubscribetosource
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{subscriberId}/unsubscribetosource")]
        public async Task<IActionResult> UnsubscribeSourceSubscription(Guid subscriberId, [FromBody] UnsubscribeToSourceCommand unsubscribeToSourceCommand)
        {
            if (unsubscribeToSourceCommand == null || subscriberId == null)
            {
                return BadRequest();
            }

            SourceSubscription sourceSubscription = null;
            int changes = 0;

            try
            {
                sourceSubscription = await _context.SourceSubscription.Where(s => s.TemblarUserId == subscriberId).Where(s => s.Source.SourceName == unsubscribeToSourceCommand.Source).FirstOrDefaultAsync();
                if (sourceSubscription == null)
                {
                    return NotFound();
                }

                _context.SourceSubscription.Remove(sourceSubscription);

                changes = await _context.SaveChangesAsync();

                if (changes == 0)
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}