﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temblar.Shared.Models;

namespace Temblar.Server.Controllers
{
    [AllowAnonymous]
    [Route("/api/[controller]")]
    [ApiController]
    public class TemblarUsersController : ControllerBase
    {
        private readonly TemblarDbContext _context;

        public TemblarUsersController(TemblarDbContext context)
        {
            _context = context;
        }

        [HttpDelete("clear")]
        public async Task<ActionResult> ClearTemblarUsers()
        {
            _context.TemblarUser.RemoveRange(_context.TemblarUser);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/TemblarUsers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TemblarUser>>> GetTemblarUser()
        {
            return await _context.TemblarUser.ToListAsync();
        }

        // GET: api/TemblarUsers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TemblarUser>> GetTemblarUser(Guid id)
        {
            var temblarUser = await _context.TemblarUser.FindAsync(id);

            if (temblarUser == null)
            {
                return NotFound();
            }

            return temblarUser;
        }

        // PUT: api/TemblarUsers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTemblarUser(Guid id, TemblarUser temblarUser)
        {
            if (id != temblarUser.Id)
            {
                return BadRequest();
            }

            _context.Entry(temblarUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemblarUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TemblarUsers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TemblarUser>> PostTemblarUser(TemblarUser temblarUser)
        {
            _context.TemblarUser.Add(temblarUser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTemblarUser", new { id = temblarUser.Id }, temblarUser);
        }

        // DELETE: api/TemblarUsers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TemblarUser>> DeleteTemblarUser(Guid id)
        {
            var temblarUser = await _context.TemblarUser.FindAsync(id);
            if (temblarUser == null)
            {
                return NotFound();
            }

            _context.TemblarUser.Remove(temblarUser);
            await _context.SaveChangesAsync();

            return temblarUser;
        }

        private bool TemblarUserExists(Guid id)
        {
            return _context.TemblarUser.Any(e => e.Id == id);
        }
    }
}