﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Temblar.Server.Migrations
{
    public partial class Initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EventType",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TemblarUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemblarUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DisplayName = table.Column<string>(nullable: true),
                    EventId = table.Column<Guid>(nullable: false),
                    EventTypeId = table.Column<Guid>(nullable: true),
                    Payload = table.Column<string>(nullable: true),
                    TriggeredBy = table.Column<string>(nullable: true),
                    TriggeredAt = table.Column<DateTimeOffset>(nullable: false),
                    DisplayMessage = table.Column<string>(nullable: true),
                    UserInterfaceUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Event_EventType_EventTypeId",
                        column: x => x.EventTypeId,
                        principalTable: "EventType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription",
                columns: table => new
                {
                    TemlbarUserId = table.Column<Guid>(nullable: false),
                    EventTypeId = table.Column<Guid>(nullable: false),
                    TemblarUserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription", x => new { x.TemlbarUserId, x.EventTypeId });
                    table.ForeignKey(
                        name: "FK_Subscription_EventType_EventTypeId",
                        column: x => x.EventTypeId,
                        principalTable: "EventType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subscription_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EventId = table.Column<Guid>(nullable: false),
                    TemlbarUserId = table.Column<Guid>(nullable: false),
                    TemblarUserId = table.Column<Guid>(nullable: true),
                    SeenAt = table.Column<DateTimeOffset>(nullable: false),
                    ReadAt = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notification_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Notification_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Event_EventTypeId",
                table: "Event",
                column: "EventTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Notification_EventId",
                table: "Notification",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Notification_TemblarUserId",
                table: "Notification",
                column: "TemblarUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_EventTypeId",
                table: "Subscription",
                column: "EventTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_TemblarUserId",
                table: "Subscription",
                column: "TemblarUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Notification");

            migrationBuilder.DropTable(
                name: "Subscription");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "TemblarUser");

            migrationBuilder.DropTable(
                name: "EventType");
        }
    }
}
