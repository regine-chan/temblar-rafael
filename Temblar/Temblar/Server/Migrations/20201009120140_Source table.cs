﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Temblar.Server.Migrations
{
    public partial class Sourcetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Source",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SourceName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Source", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SourceSubscription",
                columns: table => new
                {
                    SourceId = table.Column<Guid>(nullable: false),
                    TemblarUserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SourceSubscription", x => new { x.TemblarUserId, x.SourceId });
                    table.ForeignKey(
                        name: "FK_SourceSubscription_Source_SourceId",
                        column: x => x.SourceId,
                        principalTable: "Source",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SourceSubscription_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SourceSubscription_SourceId",
                table: "SourceSubscription",
                column: "SourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SourceSubscription");

            migrationBuilder.DropTable(
                name: "Source");
        }
    }
}
