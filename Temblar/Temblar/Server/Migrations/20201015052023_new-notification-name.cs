﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Temblar.Server.Migrations
{
    public partial class newnotificationname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Notification");

            migrationBuilder.CreateTable(
                name: "CustomNotification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    EventId = table.Column<Guid>(nullable: false),
                    TemlbarUserId = table.Column<Guid>(nullable: false),
                    TemblarUserId = table.Column<Guid>(nullable: true),
                    SeenAt = table.Column<DateTimeOffset>(nullable: true),
                    ReadAt = table.Column<DateTimeOffset>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomNotification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomNotification_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomNotification_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomNotification_EventId",
                table: "CustomNotification",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomNotification_TemblarUserId",
                table: "CustomNotification",
                column: "TemblarUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomNotification");

            migrationBuilder.CreateTable(
                name: "Notification",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EventId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReadAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    SeenAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    TemblarUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    TemlbarUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notification_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Notification_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Notification_EventId",
                table: "Notification",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Notification_TemblarUserId",
                table: "Notification",
                column: "TemblarUserId");
        }
    }
}
