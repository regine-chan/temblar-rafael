﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Temblar.Server.Migrations
{
    public partial class pushnotificationsupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientDevice",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TemblarUserId = table.Column<Guid>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    P256dh = table.Column<string>(nullable: true),
                    Auth = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false, defaultValue: new DateTimeOffset(new DateTime(2020, 10, 18, 1, 55, 2, 852, DateTimeKind.Unspecified).AddTicks(3953), new TimeSpan(0, 2, 0, 0, 0)))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientDevice_TemblarUser_TemblarUserId",
                        column: x => x.TemblarUserId,
                        principalTable: "TemblarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientDevice_TemblarUserId",
                table: "ClientDevice",
                column: "TemblarUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientDevice");
        }
    }
}
