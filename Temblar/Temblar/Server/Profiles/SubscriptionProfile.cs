﻿using AutoMapper;
using System.Linq;
using Temblar.Shared.Models;
using Temblar.Shared.Commands;
using Temblar.Shared.DTOs;

namespace Temblar.Shared.Profiles
{
    public class SubscriptionProfile : Profile
    {
       public SubscriptionProfile()
        {
            /*CreateMap<SourceSubscription, PersonalSubscriberDTO>()
                .ForMember(s => s.SubscribedSources, opt => opt
                .MapFrom(s => s.Source)
                );
            */
            CreateMap<TemblarUser, PersonalSubscriberDTO>()
                .ForMember(tu => tu.SubscribedSources, opt => opt
                .MapFrom(ss => ss.SourceSubscriptions
                .Select(s => s.Source)
                .ToHashSet()
                ));




           // CreateMap<Subscription, PersonalSubscriberDTO>();
        }
    }
}
