using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.Identity.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Identity.Web.UI;
using Microsoft.IdentityModel.Logging;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;
using Temblar.Server.Handlers;
using Coravel;
using Coravel.Invocable;
using Coravel.Scheduling.Schedule.Cron;
using System;
using Microsoft.DotNet.PlatformAbstractions;
using System.Reflection;
using System.IO;

namespace Temblar.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
            this.env = env;
        }

        public IConfiguration Configuration { get; }

        public IHostEnvironment env { get;}

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
            if (env.IsDevelopment())
            {
                services.AddDbContext<TemblarDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Default")));
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddIdentityServerAuthentication(opt => {
                    opt.Authority = "https://login.microsoftonline.com/ead5a9b8-4a2d-4d79-b8f5-3beff6730b27/V2.0";

                });
                services.AddCors(options =>
                {
                    options.AddPolicy("CorsPolicy", builder =>
                    {
                        // builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                        builder.WithOrigins("https://regine-chan.gitlab.io").AllowAnyHeader().AllowAnyMethod();
                    });
                });
            }
            else
            {
                services.AddDbContext<TemblarDbContext>(opt => opt.UseSqlServer(Environment.GetEnvironmentVariable("DB_URL")));
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddIdentityServerAuthentication(opt => {
                    opt.Authority = Environment.GetEnvironmentVariable("AUTHORITY");

                });
                services.AddCors(options =>
                {
                    options.AddPolicy("CorsPolicy", builder =>
                    {
                        // builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                        builder.WithOrigins(Environment.GetEnvironmentVariable("ADMIN_ORIGIN")).AllowAnyHeader().AllowAnyMethod();
                    });
                });
            }
            
            services.AddAuthorization();
            services.AddSignalR();
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddSwaggerGen(c => 
            {
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            

            services.AddControllersWithViews(options => 
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            })
                .AddMicrosoftIdentityUI()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 );

            
            services.AddTransient<AppBadgeHandler>();
            services.AddScheduler();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();


            app.ApplicationServices.UseScheduler(scheduler => 
            {
                scheduler.Schedule<AppBadgeHandler>().Cron("00 */3 * * *");
            }); 

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<NotificationHub>("/notificationshub");
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
