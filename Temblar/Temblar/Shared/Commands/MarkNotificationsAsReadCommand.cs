﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.Commands
{
    public class MarkNotificationsAsReadCommand
    {
        /// <summary>
        /// Ids of notifications that should be marked as read
        /// </summary>
        public List<Guid> ReadIds { get; set; } = new List<Guid>();
    }
}
