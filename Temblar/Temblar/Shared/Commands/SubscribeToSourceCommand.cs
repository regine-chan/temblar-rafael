﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.Commands
{
    public class SubscribeToSourceCommand
    {
        public string Source { get; set; }
    }
}
