﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Temblar.Shared.Models
{
    public class CustomNotification
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public Guid EventId { get; set; }

        public Event Event { get; set; }

        public Guid TemlbarUserId { get; set; }

        public TemblarUser TemblarUser { get; set; }

        public DateTimeOffset? SeenAt { get; set; }

        public DateTimeOffset? ReadAt { get; set; }

    }
}
