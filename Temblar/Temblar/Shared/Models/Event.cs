﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Temblar.Shared.Models
{
    public class Event
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public Guid? EventTypeId { get; set; }

        public EventType? EventType { get; set; }

        public Guid? SourceId { get; set; }

        public Source? Source { get; set; }

        // JSON string
        public string Payload { get; set; }

        public string TriggeredBy { get; set; }

        public DateTimeOffset TriggeredAt { get; set; }

        public string DisplayMessage { get; set; }

        public string UserInterfaceUrl { get; set; }

        [JsonIgnore]
        public IList<CustomNotification> CustomNotifications { get; set; }
    }
}
