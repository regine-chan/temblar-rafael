﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Temblar.Shared.Models
{
    public class SourceSubscription
    {
        public Guid SourceId { get; set; }
        public Source Source { get; set; }

        public Guid TemblarUserId { get; set; }

        public TemblarUser TemblarUser { get; set; }
    }
}
