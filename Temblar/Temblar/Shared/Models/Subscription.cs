﻿using System;


namespace Temblar.Shared.Models
{
    public class Subscription
    {

        public Guid TemblarUserId { get; set; }

        public TemblarUser TemblarUser { get; set; }

        public Guid EventTypeId { get; set; }

        public EventType EventType { get; set; }
    }
}
