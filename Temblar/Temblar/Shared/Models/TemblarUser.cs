﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Temblar.Shared.Models
{
    public class TemblarUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string Username { get; set; }

        [JsonIgnore]
        public IList<Subscription> Subscriptions { get; set; }

        [JsonIgnore]
        public IList<CustomNotification> CustomNotifications { get; set; }

        [JsonIgnore]
        public IList<SourceSubscription> SourceSubscriptions { get; set; }

        [JsonIgnore]
        public IList<ClientDevice> ClientDevices { get; set; }
    }
}
